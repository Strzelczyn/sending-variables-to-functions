
#include <iostream>

//copy
void foo(int i)
{
    ++i;
}

//reference C
void foo1(int *i)
{
    (*i)++;
}

//pointer
void foo2(int **p)
{
    (*(*p))++;
}

//reference C++
void foo3(int &i)
{
    i++;
}

int main()
{
    int i = 3;
    int *p = &i;
    foo(i);
    std::cout << i << std::endl;
    foo3(i);
    std::cout << i << std::endl;
    foo1(&i);
    std::cout << i << std::endl;
    foo2(&p);
    std::cout << i << std::endl;
}